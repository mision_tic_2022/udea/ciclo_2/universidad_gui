
package com.universidad_gui.universidad_gui.modelo;

public class Universidad {
    //ATRIBUTOS
    private String nombre;
    private String telefono;
    private String nit;
    private String direccion;
    
    //CONSTRUCTOR
    public Universidad(String nombre, String telefono, String nit, String direccion){
        this.nombre = nombre;
        this.telefono = telefono;
        this.nit = nit;
        this.direccion = direccion;
    }
    
    //CONSULTORES

    public String getNombre() {
        return nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getNit() {
        return nit;
    }

    public String getDireccion() {
        return direccion;
    }
    
    //MODIFICADORES

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
}
